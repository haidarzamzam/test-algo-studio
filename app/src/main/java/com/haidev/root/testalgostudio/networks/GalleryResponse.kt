package com.haidev.root.testalgostudio.networks

import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import io.reactivex.Observable

class GalleryResponse (private val apiService:RestApi) {

    fun getListImage(): Observable<GalleryModel> {
        return apiService.getListImage()
    }
}