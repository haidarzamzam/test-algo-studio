package com.haidev.root.testalgostudio.menus.gallery.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.haidev.root.testalgostudio.R
import com.haidev.root.testalgostudio.databinding.FragmentGalleryBinding
import com.haidev.root.testalgostudio.menus.gallery.adapters.ItemGalleryAdapter
import com.haidev.root.testalgostudio.menus.gallery.interfaces.IvListImageRepository
import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import com.haidev.root.testalgostudio.menus.gallery.viewmodels.GalleryViewModel
import com.haidev.root.testalgostudio.utils.ApplicationHelper

class GalleryFragment : Fragment(), IvListImageRepository {
    private lateinit var galleryBinding: FragmentGalleryBinding
    private lateinit var vmGallery: GalleryViewModel

    private lateinit var itemGalleryAdapter: ItemGalleryAdapter
    private var listImage:MutableList<GalleryModel.ListData.ListImage> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        galleryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false)
        vmGallery = GalleryViewModel(this)
        galleryBinding.gallery = vmGallery

        vmGallery.requestListImage()
        initSwipeRefresh()
        initRecyclerView()
        return galleryBinding.root
    }

    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        itemGalleryAdapter = ItemGalleryAdapter(context!!, listImage)
        galleryBinding.rvGallery.layoutManager = layoutManager
        galleryBinding.rvGallery.adapter = itemGalleryAdapter
    }

    private fun initSwipeRefresh() {
        galleryBinding.swipe.setOnRefreshListener {
            vmGallery.requestListImage()
        }
    }

    companion object {
        fun getInstance() : GalleryFragment = GalleryFragment()
    }

    override fun onGetListImageSuccess(listImageModel: GalleryModel) {
        galleryBinding.swipe.isRefreshing = false
        if (listImageModel == null){
            Toast.makeText(context, "Kosong bro", Toast.LENGTH_SHORT).show()
        }else{
            itemGalleryAdapter.addData(listImageModel.data[0].memes as MutableList<GalleryModel.ListData.ListImage>)
            galleryBinding.rvGallery.post {
                itemGalleryAdapter.notifyDataSetChanged()
            }
        }
    }

    override fun onGetListImageError() {
        galleryBinding.swipe.isRefreshing = false
        ApplicationHelper.Helper.displayToastContext(context!!, getString(R.string.error_get_list))
    }
}
