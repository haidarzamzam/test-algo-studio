package com.haidev.root.testalgostudio.menus.camera.views

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.haidev.root.testalgostudio.R
import com.haidev.root.testalgostudio.databinding.FragmentCameraBinding
import com.haidev.root.testalgostudio.menus.camera.viewmodels.CameraViewModel
import kotlinx.android.synthetic.main.fragment_camera.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CameraFragment : Fragment() {

    private lateinit var cameraBinding: FragmentCameraBinding
    private lateinit var vmCamera: CameraViewModel


    private val REQUEST_IMAGE = 100
    private val REQUEST_PERMISSION = 200
    private var imageFilePath = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        cameraBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_camera, container, false)
        vmCamera = CameraViewModel()
        cameraBinding.camera = vmCamera

        cameraBinding.fabCamera.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION
                )
            } else {
                openCameraIntent()
            }
        }

        return cameraBinding.root
    }

    private fun openCameraIntent() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (pictureIntent.resolveActivity(activity!!.packageManager) != null) {

            var photoFile: File? = null

            try {
                photoFile = createImageFile()
            } catch (e: IOException) {
                e.printStackTrace()
                return
            }

            val photoUri = FileProvider.getUriForFile(context!!, activity!!.packageName + ".provider", photoFile)
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
            startActivityForResult(pictureIntent, REQUEST_IMAGE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSION && grantResults.size > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Thanks for granting Permission", Toast.LENGTH_SHORT).show()
                openCameraIntent()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                ivCamera.setImageURI(Uri.parse(imageFilePath))
                Log.d("FilePath", imageFilePath)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(context, "You cancelled the operation", Toast.LENGTH_SHORT).show()
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val storageDir = File(Environment.getExternalStorageDirectory().toString() + "/Haidar")

        if (!storageDir.exists()) {
            storageDir.mkdirs()
        } else {
            Toast.makeText(context, "Sudah ada folder", Toast.LENGTH_SHORT).show()
        }

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "IMG_" + timeStamp + "_"
        val image = File.createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = image.absolutePath

        return image
    }

    companion object {
        fun getInstance() : CameraFragment = CameraFragment()
    }
}
