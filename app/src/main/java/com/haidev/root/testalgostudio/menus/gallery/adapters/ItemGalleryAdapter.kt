package com.haidev.root.testalgostudio.menus.gallery.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.haidev.root.testalgostudio.R
import com.haidev.root.testalgostudio.databinding.ItemGalleryLayoutBinding
import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import com.haidev.root.testalgostudio.menus.gallery.viewmodels.ItemGalleryViewModel

class ItemGalleryAdapter (val context: Context, private var myListBrand:MutableList<GalleryModel.ListData.ListImage>): RecyclerView.Adapter<ItemGalleryAdapter.MyViewHolder>() {
    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  ItemGalleryAdapter.MyViewHolder {
        val itemGalleryLayoutBinding: ItemGalleryLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.item_gallery_layout,parent,false)
        return MyViewHolder(context, itemGalleryLayoutBinding)
    }

    override fun getItemCount(): Int {
        return myListBrand.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val fixPos = holder.adapterPosition
        holder.bindBinding(myListBrand[fixPos])
    }

    fun addData(mutableList: MutableList<GalleryModel.ListData.ListImage>){
        this.myListBrand.clear()
        this.myListBrand.addAll(mutableList)
    }

    class MyViewHolder(val context: Context, itemView: ItemGalleryLayoutBinding) : RecyclerView.ViewHolder(itemView.root) {
        private val itemListGalleryBinding= itemView

        fun bindBinding(model:GalleryModel.ListData.ListImage){
            val itemListTripViewModel = ItemGalleryViewModel(context, model, itemListGalleryBinding)
            itemListGalleryBinding.itemGallery = itemListTripViewModel
        }
    }
}