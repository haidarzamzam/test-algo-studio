package com.haidev.root.testalgostudio.menus.gallery.viewmodels

import android.content.Context
import com.haidev.root.testalgostudio.databinding.ItemGalleryLayoutBinding
import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import com.squareup.picasso.Picasso

class ItemGalleryViewModel(val context: Context, listImage: GalleryModel.ListData.ListImage, binding: ItemGalleryLayoutBinding) {

    init{
        Picasso.get().load(listImage.url).into(binding.ivGallery)
        binding.txtGallery.text = listImage.name
    }
}