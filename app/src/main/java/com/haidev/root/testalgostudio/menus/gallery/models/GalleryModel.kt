package com.haidev.root.testalgostudio.menus.gallery.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GalleryModel (
    @Expose @SerializedName("success") val status:String,
    @Expose @SerializedName("data") val data:List<ListData>) {

    class ListData(
        @Expose @SerializedName("memes") val memes: List<ListImage>
    ) {

        class ListImage(
            @Expose @SerializedName("id") val id:String,
            @Expose @SerializedName("name") val name:String,
            @Expose @SerializedName("url") val url:String,
            @Expose @SerializedName("width") val width:String,
            @Expose @SerializedName("height") val height:String
        )
    }
}

