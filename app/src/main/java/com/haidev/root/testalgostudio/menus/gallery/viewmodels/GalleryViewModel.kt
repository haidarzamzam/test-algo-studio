package com.haidev.root.testalgostudio.menus.gallery.viewmodels

import com.haidev.root.testalgostudio.menus.gallery.interfaces.IvListImageRepository
import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import com.haidev.root.testalgostudio.networks.GalleryResponse
import com.haidev.root.testalgostudio.networks.ResponseRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GalleryViewModel(private val ivListImageRepository:IvListImageRepository) {

    private val galleryResponse: GalleryResponse = ResponseRepository.provideListImageRepository()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun requestListImage(){
        compositeDisposable.add(
            galleryResponse.getListImage()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                        t: GalleryModel? -> ivListImageRepository.onGetListImageSuccess(t!!)
                }, { ivListImageRepository.onGetListImageError() })
        )
    }
}