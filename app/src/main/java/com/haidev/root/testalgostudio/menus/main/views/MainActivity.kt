package com.haidev.root.testalgostudio.menus.main.views

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.haidev.root.testalgostudio.R
import com.haidev.root.testalgostudio.databinding.ActivityMainBinding
import com.haidev.root.testalgostudio.menus.camera.views.CameraFragment
import com.haidev.root.testalgostudio.menus.gallery.views.GalleryFragment
import com.haidev.root.testalgostudio.menus.main.viewmodels.MainViewModel
import com.haidev.root.testalgostudio.utils.BottomNavigationViewHelper

class MainActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityMainBinding
    private lateinit var vmMain: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        vmMain = MainViewModel()
        mainBinding.main = vmMain

        supportFragmentManager.beginTransaction().replace(R.id.rootFragment, GalleryFragment.getInstance()).commit()
        initNavBottom()
    }

    private fun initNavBottom() {
        mainBinding.bottomView.itemIconTintList = null
        BottomNavigationViewHelper.disableShiftMode(mainBinding.bottomView)
        mainBinding.bottomView.setOnNavigationItemSelectedListener {
            when {
                it.itemId == R.id.galleryMenu -> {
                    supportFragmentManager.beginTransaction().replace(R.id.rootFragment, GalleryFragment.getInstance())
                        .commit()
                    mainBinding.txtToolbar.text = getString(R.string.string_gallery_menu)
                }
                it.itemId == R.id.cameraMenu -> {
                    supportFragmentManager.beginTransaction().replace(R.id.rootFragment, CameraFragment.getInstance())
                        .commit()
                    mainBinding.txtToolbar.text = getString(R.string.string_camera_menu)
                }
                else -> supportFragmentManager.beginTransaction().replace(
                    R.id.rootFragment,
                    GalleryFragment.getInstance()
                ).commit()
            }
            return@setOnNavigationItemSelectedListener true
        }
    }
}
