package com.haidev.root.testalgostudio.utils

import android.app.Activity
import android.content.Context
import android.widget.Toast

class ApplicationHelper {
    companion object Helper {
        fun displayToast(activity: Activity, message: String) {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        }

        fun displayToastContext(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }
    }
}