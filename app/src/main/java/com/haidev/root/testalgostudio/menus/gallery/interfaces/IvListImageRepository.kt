package com.haidev.root.testalgostudio.menus.gallery.interfaces

import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel

interface IvListImageRepository {
    fun onGetListImageSuccess(listImageModel: GalleryModel)
    fun onGetListImageError()
}