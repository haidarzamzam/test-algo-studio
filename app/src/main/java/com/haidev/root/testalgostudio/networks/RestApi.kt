package com.haidev.root.testalgostudio.networks

import com.haidev.root.testalgostudio.datas.AppData
import com.haidev.root.testalgostudio.menus.gallery.models.GalleryModel
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface RestApi {
    companion object Factory{

        fun create(): RestApi{

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val clientBuilder = OkHttpClient.Builder()
            clientBuilder.addInterceptor(logging)
            val client = clientBuilder.build()
            val retrofit = Retrofit.Builder()
                .baseUrl(AppData.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
            return retrofit.create(RestApi::class.java)
        }
    }

    @GET("get_memes")
    fun getListImage(): Observable<GalleryModel>

}